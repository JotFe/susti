import Vue from 'vue';
import App from './App.vue';

Vue.component('tablaproducto-item', {
  props: ['item'],
  template: '<tr><td>{{item.first_name}}</td><td><img v-bind:src="item.avatar"></td></tr>'
});

new Vue({
  el: '#container',
  render: h => h(App)
})
